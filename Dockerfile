FROM composer:1.7 as vendor

COPY database/ database/
COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist

FROM node:11 as frontend

RUN mkdir -p /app/{public,resources}
COPY package.json yarn.lock webpack.mix.js /app/
COPY resources/assets/ /app/resources/assets/

WORKDIR /app

RUN yarn install && yarn production

FROM php:7.2-apache

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

RUN apt-get update && apt-get install -y curl unzip procps
RUN docker-php-ext-install mysqli pdo_mysql

RUN a2enmod rewrite

# Dependencies
COPY --from=vendor /app/vendor/ /var/www/html/vendor/
COPY --from=frontend /app/public/js/ /var/www/html/public/js/
COPY --from=frontend /app/public/css/ /var/www/html/public/css/
COPY --from=frontend /app/public/img/ /var/www/html/public/img/
#COPY --from=frontend /app/public/images/ /var/www/html/public/images/
COPY --from=frontend /app/mix-manifest.json /var/www/html/mix-manifest.json

# Main application
ADD --chown=www-data:www-data . /var/www/html
RUN touch /var/www/html/.env

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
