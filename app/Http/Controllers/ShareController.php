<?php

namespace App\Http\Controllers;

use App\Liquid;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class ShareController extends Controller
{
    public function get($liquid)
    {
        $liquid = Liquid::withoutGlobalScopes(["for-user"])->where('share_id', $liquid)->first();

        if (!$liquid) {
            abort(404);
        }
        return view("share.show", ['liquid' => $liquid]);
    }

    function store(Liquid $liquid)
    {
        if (!$liquid->share_id) {
            $liquid->share_id = Uuid::uuid4()->toString();
            $liquid->save();
        }

        return response()->json([
            'share_id' => $liquid->share_id
        ]);
    }

    function destroy(Liquid $liquid)
    {
        $liquid->share_id = null;
        $liquid->save();

        return response(null, 204);
    }
}
