<?php

namespace App\Http\Middleware;

use Closure;

class RequestTimingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $start = microtime(true);
        $res = $next($request);
        $time = microtime(true) - $start;

        logger()->channel('timing')->info('request:' . $request->path(), ['time' => $time]);
        return $res;
    }
}
