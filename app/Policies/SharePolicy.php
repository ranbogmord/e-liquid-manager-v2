<?php

namespace App\Policies;

use App\Liquid;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SharePolicy
{
    use HandlesAuthorization;

    function share(User $user, Liquid $liquid)
    {
        return $liquid->author_id === $user->id;
    }

    function unshare(User $user, Liquid $liquid)
    {
        return $liquid->author_id === $user->id;
    }
}
