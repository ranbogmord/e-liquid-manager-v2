<?php

function elm_timings($ident, $callable) {
    if (!is_callable($callable)) {
        throw new Exception('$callable is not callable');
    }

    $start = microtime(true);
    $result = $callable();
    logger()->channel("timing")->info($ident, ['time' => microtime(true) - $start]);
    return $result;
}
