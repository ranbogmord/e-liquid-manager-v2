window.Vue = require('vue');
const moment = require('moment');

Vue.filter('date', (val, format="YYYY-MM-DD HH:mm:ss") => {
  return moment(val).local().format(format);
});
