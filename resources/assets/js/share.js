const initApp = function () {
  require('./bootstrap');
  window.Vue = require('vue');

  window.ELM.liquid = Object.assign(window.ELM.liquid, {
    base_pg_percentage: 100,
    base_vg_percentage: 0,
    batch_size: 30,
  });

  const shareApp = new Vue({
    el: "#app",
    data() {
      return {
        liquid: window.ELM.liquid
      };
    },
    watch: {
      'liquid.base_pg_percentage'(val) {
        this.liquid.base_vg_percentage = 100 - (+val);
      },
      'liquid.base_vg_percentage'(val) {
        this.liquid.base_pg_percentage = 100 - (+val);
      },
      'liquid.target_pg_percentage'(val) {
        this.liquid.target_vg_percentage = 100 - (+val);
      },
      'liquid.target_vg_percentage'(val) {
        this.liquid.target_pg_percentage = 100 - (+val);
      },
    },
    components: {
      'mix-table': require('./components/MixTable')
    }
  })
};


if (window.Raven) {
  Raven.config('https://4f2d6206ed4e4e4a90205805a07072b8@sentry.io/304736').install();
  Raven.context(() => {
    initApp();
  });
} else {
  initApp();
}