const InitArchived = () => {
  require('./bootstrap');
  require('./vue');


  const ArchivedApp = new Vue({
    el: "#archived-liquids",
    components: {
      'archived-liquids': require('./components/ArchivedLiquids')
    }
  });
};

if (window.Raven) {
  Raven.config('https://4f2d6206ed4e4e4a90205805a07072b8@sentry.io/304736').install();
  Raven.context(() => {
    InitArchived();
  });
} else {
  InitArchived();
}
