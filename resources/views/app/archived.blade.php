@extends("layouts.app")

@section("styles")
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('css/archived.css') }}">
@append

@section("content")
    <div id="archived-liquids" class="edit-page">
        <h1>Archived liquids</h1>
        <archived-liquids></archived-liquids>
    </div>
@endsection

@section("scripts")
    <script src="{{ asset('js/archived.js') }}"></script>
    <script src="{{ asset('js/ui.js') }}"></script>
@endsection
