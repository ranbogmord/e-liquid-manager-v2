<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>E-Liquid Manager</title>

    <link rel="stylesheet" href="{{ asset('css/base.css') }}">
    @yield("styles")
</head>
<body>
    @if(!request()->is("login", "password/*", "register"))
        @include("partials.nav")
    @endif
    @include("partials.messages")

    @yield("content")

    @if(config('sentry.dsn'))
        <script src="https://cdn.ravenjs.com/3.23.3/raven.min.js" crossorigin="anonymous"></script>
        @if(auth()->user())
        <script>
            Raven.setUserContext({
              email: '{{ auth()->user()->email }}',
              id: '{{ auth()->id() }}'
            })
        </script>
        @endif
    @endif
    <script>
        window.ELM = {
          url: '{{ config('app.url') }}'
        };
    </script>
    @yield("scripts")
</body>
</html>
