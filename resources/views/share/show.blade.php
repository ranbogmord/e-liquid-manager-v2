@extends("layouts.app")

@section("styles")
    <link rel="stylesheet" href="{{ asset("css/share.css") }}">
@endsection

@section("content")
    <div id="app">
        <h1>{{ $liquid->name }}</h1>
        <div id="params">
            <div id="base">
                <h2>Base</h2>
                <div class="fields">
                    <div class="field">
                        <label for="">
                            Base nicotine strength <br>
                            <input type="number" v-model="liquid.base_nic_strength" step="1" min="0">
                        </label>
                    </div>
                    <div class="field">
                        <label for="">
                            Base nicotine PG % <br>
                            <input type="number" v-model="liquid.base_pg_percentage" step="1" min="0">
                        </label>
                    </div>
                    <div class="field">
                        <label for="">
                            Base nicotine VG % <br>
                            <input type="number" v-model="liquid.base_vg_percentage" step="1" min="0">
                        </label>
                    </div>
                </div>
            </div>
            <div id="target">
                <h2>Target</h2>
                <div class="fields">
                    <div class="field">
                        <label for="">
                            Batch size <br>
                            <input type="number" v-model="liquid.batch_size" step="1" min="0">
                        </label>
                    </div>
                    <div class="field">
                        <label for="">
                            Target nicotine strength <br>
                            <input type="number" v-model="liquid.target_nic_strength" step="0.5" min="0">
                        </label>
                    </div>
                    <div class="field">
                        <label for="">
                            Target PG % <br>
                            <input type="number" v-model="liquid.target_pg_percentage" step="1" min="0">
                        </label>
                    </div>
                    <div class="field">
                        <label for="">
                            Target VG % <br>
                            <input type="number" v-model="liquid.target_vg_percentage" step="1" min="0">
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <mix-table :liquid="liquid"></mix-table>
    </div>
@endsection

@section("scripts")
    <script>
      window.ELM.liquid = {!! $liquid !!};
    </script>
    <script src="{{ asset("js/share.js") }}"></script>
@endsection