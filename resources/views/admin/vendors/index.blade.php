@extends("layouts.admin")

@section("content")
    <div class="index-page">
        <h1>Vendors</h1>

        <a href="{{ route('admin.vendors.create') }}">New Vendor</a>

        <v-datatable id="vue-datatable" :headers="[{name: 'id', label: 'ID'}, {name: 'name', label: 'Name'}, {name: 'abbr', label: 'Abbreviation'}, {name: 'created_at', label: 'Created At'}, {name: 'updated_at', label: 'Updated At'}]" :rows="{{ $items }}"></v-datatable>

{{--        <table class="datatable">--}}
{{--            <thead>--}}
{{--            <tr>--}}
{{--                <th>ID</th>--}}
{{--                <th>Name</th>--}}
{{--                <th>Abbreviation</th>--}}
{{--                <th>Created at</th>--}}
{{--                <th>Updated at</th>--}}
{{--            </tr>--}}
{{--            </thead>--}}

{{--            <tbody>--}}
{{--            @foreach($items as $item)--}}
{{--                <tr>--}}
{{--                    <td>--}}
{{--                        <a href="{{ route('admin.vendors.edit', $item->id) }}">{{ $item->id }}</a>--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        <a href="{{ route('admin.vendors.edit', $item->id) }}">{{ $item->name }}</a>--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        <a href="{{ route('admin.vendors.edit', $item->id) }}">{{ $item->abbr }}</a>--}}
{{--                    </td>--}}
{{--                    <td>{{ $item->created_at }}</td>--}}
{{--                    <td>{{ $item->updated_at }}</td>--}}
{{--                </tr>--}}
{{--            @endforeach--}}
{{--            </tbody>--}}
{{--        </table>--}}
    </div>
@endsection
