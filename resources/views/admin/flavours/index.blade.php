@extends("layouts.admin")

@section("content")
    <div class="index-page">
        <h1>Flavours</h1>

        <a href="{{ route('admin.flavours.create') }}">New Flavour</a>

        <v-datatable id="vue-datatable" :headers="[{name: 'id', label: 'ID'}, {name: 'name', label: 'Name'}, {name: 'vendor.name', label: 'Vendor'}, {name: 'created_at', label: 'Created At'}, {name: 'updated_at', label: 'Updated At'}]" :rows="{{ $items }}"></v-datatable>

{{--        <table id="vue-datatable" :items="{{ $items }}">--}}
{{--            <thead>--}}
{{--            <tr>--}}
{{--                <th @click.prevent="sort('id')">ID</th>--}}
{{--                <th @click.prevent="sort('name')">Name</th>--}}
{{--                <th @click.prevent="sort('vendor')">Vendor</th>--}}
{{--                <th @click.prevent="sort('created_at')">Created at</th>--}}
{{--                <th @click.prevent="sort('updated_at')">Updated at</th>--}}
{{--            </tr>--}}
{{--            </thead>--}}

{{--            <tbody>--}}
{{--            @foreach($items as $item)--}}
{{--                <tr v-for="item in items">--}}
{{--                    <td>--}}
{{--                        <a href="{{ route('admin.flavours.edit', $item->id) }}">{{ $item->id }}</a>--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        <a href="{{ route('admin.flavours.edit', $item->id) }}">{{ $item->name }} ({{ array_get($item, 'vendor.abbr', 'Other') }})</a>--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        <a href="{{ route('admin.flavours.edit', $item->id) }}">{{ array_get($item, 'vendor.name', 'Other') }}</a>--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        {{ $item->created_at }}--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        {{ $item->updated_at }}--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--            @endforeach--}}
{{--            </tbody>--}}
{{--        </table>--}}
    </div>
@endsection
