<div class="submit-field">
    <input type="submit" value="{{ $text ?? "Submit" }}" class="{{ $class ?? "" }}">
</div>
