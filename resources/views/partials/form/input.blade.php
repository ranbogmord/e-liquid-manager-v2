<div class="input-field">
    <label>
        {{ $label }}: <br>
        <input type="{{ $type ?? "text" }}" name="{{ $name ?? str_slug($label) }}" value="{{ $value ?? "" }}">
    </label>
</div>
